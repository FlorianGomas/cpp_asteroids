#pragma once
#include "SpaceElement.h"
#include "Space.h"

class Background : public SpaceElement
{
	public:
		explicit Background(Space& p_space);
		virtual void collision(ElementType otherType) override;
	private:
		Space& space;

};

