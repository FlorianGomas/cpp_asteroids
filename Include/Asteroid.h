#pragma once
#include "SpaceElement.h"
#include "Space.h"
#include "Game.h"


class Asteroid : public SpaceElement
{
	public:
		explicit Asteroid(Game& p_game, Space& p_space, Asteroid *parent = nullptr);
		virtual void collision(ElementType otherType) override;
	private:
		Space& space;
		Game& game;
};

