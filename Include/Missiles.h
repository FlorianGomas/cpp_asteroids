#pragma once
#include "SpaceElement.h"
#include <SFML/Audio.hpp>

class Missiles : public SpaceElement
{
	public:
		Missiles(Coordinates const& p_position, float rotation);
		virtual void collision(ElementType otherType) override;

	private:
		static constexpr float SPEED{ 2000.f };
};

