#pragma once
#include <exception>
#include <string_view>
#include <string>
class ExceptionResourceNotFound : public std::exception
{
	public:
		ExceptionResourceNotFound(std::string_view const & path);
		virtual const char* what() const noexcept override;
	private:
		std::string errorMessage{};
};

