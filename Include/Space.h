#pragma once
#include <SFML/Graphics.hpp>
#include "SpaceElement.h"
#include <vector>

class Space
{
	public:
		Space();
		~Space()=default;
		void add(std::unique_ptr<SpaceElement> element);
		void update();
		void collision();
		void display(sf::RenderWindow& window) const;
		void cleanup();
		void clear();
		inline bool isClear() const { return elements.empty() && toAdd.empty(); };


	private:
		std::vector <std::unique_ptr<SpaceElement>> elements{};
		std::vector <std::unique_ptr<SpaceElement>> toAdd{};
		sf::Clock chrono{};
		bool toClear{ false };


};

