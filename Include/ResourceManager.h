#pragma once
#include <unordered_map>
#include <string_view>
#include <iostream>
#include "ExceptionResourceNotFound.h"


// L'utilisaltion d'un template permet d'utiliser n'importe qu'elle type de varible avec cette classe/fonction (Ici sf::Texture et/ou sf::Sound remplacer par "T").
// Attention cependant lorsqu'on utilise un template, on est oblig� de d�finir l'ensemble de la fonction/classe dans le fichier header.
template <typename T>
class ResourceManager
{
	public:
		ResourceManager() = delete;
		static T const& getResource(std::string_view const& path) {
			auto result = resources.find(path);
			if (result == std::end(resources)) {
				//path.data() retourne le chaine de caract�re qui se cache derri�re string view (rappel string view est une vue vers une chaine, mais pas la chaine en elle m�me)
				if (!resources[path].loadFromFile(path.data())) {
					//Permet de lancer l'erreur pour �ventuellement l'attraper si un bloque try and catch est pr�sent
					throw  ExceptionResourceNotFound{ path };
				}
				return resources[path];
			}
			return result->second;
		}

	private:
		static std::unordered_map<std::string_view, T> resources;
};

