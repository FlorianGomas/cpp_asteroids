#pragma once

#include "Vector.h"

class Coordinates
{
	private:
		float y{ heightLimit/2.f };
		float x{ widthLimit/2.f };
		static int widthLimit;
		static int heightLimit;

	public:
		Coordinates() = default;
		Coordinates(float pX, float pY);
		float getX();
		float getY();
		static int getWidthLimit();
		static int getHeightLimit();
		void operator+=(Vector const& other);
		static void initializeSpace(int const& WIDTH, int const& HEIGHT);
		void calculate();
		float distanceCalculation(Coordinates const& other) const;
};

 