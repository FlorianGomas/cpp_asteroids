#pragma once
#include "Vector.h"
#include "Coordinates.h"
#include <SFML/Graphics.hpp>
#include <string_view>

enum class ElementType {SHIP, ASTEROID, MISSILES, BACKGROUND ,OTHERS};

class SpaceElement
{
	public:
		virtual ~SpaceElement() = default;
		explicit SpaceElement(std::string_view const& imagePath);
		//Suppression du constructeur du copie qui permettai de construire un objet en copiant un objet existant
		SpaceElement(SpaceElement const& other) = delete;
		//Suppression de l'op�rateur de copie
		void operator=(SpaceElement const& other) = delete;
		/*"virtual" permet de pr�ciser au compilateur que lorsqu'on appel la m�thode update de spaceElement, les liens doivent �tre fait de mani�re dynamique (et non statique par d�faut).
		Cel� permet d'appeler la m�thode sur le type r��l (dynamique) de l'objet. Dans notre cas, cel� permet d'utiliser la m�thode "update" de la classe "Ship" en l'appelant via "spaceElement".
		*/
		virtual void update(float duration);
		virtual void display(sf::RenderWindow& window) const;
		void collisionTest(SpaceElement& other);
		float getRadius() const;
		virtual void collision(ElementType type) = 0;
		static inline bool isDestroy(std::unique_ptr<SpaceElement>& element) { return element->destroy; };

	protected:
		bool destroy = false;
		ElementType type{ ElementType::OTHERS };
		sf::Sprite spaceElementSprite{};
		Coordinates position{};
		Vector speed{ 0.f, 0.f };
		float angularSpeed{};

};

