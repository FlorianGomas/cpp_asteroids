#pragma once

/*Classe de type "struct" : les variables d�clar� sont par d�faut public au lieu de private comme dans une classe de type "class".
Cela permet de simplement d�clar� une classe de donn�es par exemple (comme ici).
*/


struct Vector{
	//Surcharge d'op�rateur, car les op�rateur "+=", "-=", "*" etc, ne sont pas d�finie pour notre classe.
	void operator+=(Vector const& other);
	void operator-=(Vector const& other);
	Vector operator*(float coefficient) const;
	static Vector directionAngle(float size, float angle);
	float x{ 0.f };
	float y{ 0.f };
};

