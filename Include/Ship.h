#pragma once
#include "SpaceElement.h"
#include "Explosion.h"
#include "Space.h"
#include "Game.h"
#include <SFML/Audio.hpp>

class Ship : public SpaceElement {
public:
	//Constructeur par defaut
	Ship() = default; 
	/*Constructeur avec argument / Param�tre en "r�f�rence constante : Le param�tre ne sera pas modifi� dans la m�thode.
	  Explicit permet d'interdire au compilateur d'utiliser ce constructeur de mani�re implicite
	*/
	explicit Ship(Game &p_game, Space& p_space, sf::Color const &color);
	//Destructeur
	~Ship();
	
	//Surcharge de m�thode
	virtual void update(float duration) override;
	virtual void collision(ElementType otherType) override;

private :
	void refreshState();
	bool accelerating = false; //2nd mani�re d'initialiser (classique)
	bool rightTurn = false;
	bool leftTurn = false;
	bool backward = false;
	Game& game;
	Space& space;
	sf::Clock lastShot{};
	sf::Sound laserSound{};
	//Constante de classe dont la valeur est d�finie d�s la compilation
	static constexpr float ACCELERATION{ 2500.f };
	static constexpr float FRICTION_COEFF{ 2.f };
	static constexpr float ANGULAR_SPEED{ 180.f };



};