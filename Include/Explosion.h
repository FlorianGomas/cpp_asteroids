#pragma once
#include "SpaceElement.h"
#include <SFML/Audio.hpp>

class Explosion : public SpaceElement
{
	public :
		Explosion(Coordinates const& p_position);
		virtual void update(float duration) override;
		virtual void collision(ElementType otherType) override;


	private:
		sf::Sound sound{};
		float age{};
		static constexpr float LIFETIME{ 0.2f };
};

