#pragma once
#include "Space.h"
#include <SFML/Graphics.hpp>
#include <memory>
#include <exception>

class Game
{
	public:
		Game(Space &p_space);
		void start();
		void gameOver();
		inline bool inProgress() const { return onGoing; };
		void display(sf::RenderWindow& startScreen) const;
		void initializeException(std::exception const& exception);
		void addPoints(int points);
	private:
		int score{};
		int bestScore{};
		bool onGoing{ false };
		Space& space;
		sf::Text scoreText{};
		sf::Text bestScoreText{};
		sf::Sprite startScreenSprite{};
		std::unique_ptr<sf::Text> exceptionText{ nullptr };
		sf::Font font{};
		void textRefresh();
		void refreshBestScore();
};

