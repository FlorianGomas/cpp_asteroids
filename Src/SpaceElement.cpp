#include "SpaceElement.h"
#include <iostream>
#include <array>
#include <SFML/Graphics.hpp>
#include "ResourceManager.h"

SpaceElement::SpaceElement(std::string_view const & imagePath) {
	spaceElementSprite.setTexture(ResourceManager<sf::Texture>::getResource(imagePath));
	spaceElementSprite.setOrigin(spaceElementSprite.getLocalBounds().width / 2, spaceElementSprite.getLocalBounds().height / 2);
	spaceElementSprite.setPosition(position.getX(), position.getY());
}

void SpaceElement::display(sf::RenderWindow& window) const {
	std::array displays = std::array <Vector, 9> {Vector{ -1, -1 }, Vector{ 0, -1 }, Vector{ 1, -1 },
												  Vector{ -1, 0 }, Vector{ 0, 0 }, Vector{ 1, 0 },
												  Vector{ -1, 1 }, Vector{ 0, 1 }, Vector{ 1, 1 }};
	for (Vector &display : displays) {
		sf::Transform transformation{};
		transformation.translate(display.x * Coordinates::getWidthLimit(), display.y * Coordinates::getHeightLimit());
		window.draw(spaceElementSprite, transformation);
	}
	
}

void SpaceElement::update(float duration) {
	Vector movement = speed * duration;
	position += movement;
	spaceElementSprite.setPosition(position.getX(), position.getY());
	spaceElementSprite.rotate(angularSpeed * duration);
}

float SpaceElement::getRadius() const {
	return spaceElementSprite.getGlobalBounds().height / 2.4f;
}

void SpaceElement::collisionTest(SpaceElement& other) {
	float distance = position.distanceCalculation(other.position);
	if (distance < getRadius() + other.getRadius()){
		collision(other.type);
	}
}