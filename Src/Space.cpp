#include "Space.h"
#include <iostream>
#include <vector>

Space::Space(){

}

void Space::add(std::unique_ptr<SpaceElement> element) {
	toAdd.push_back(std::move(element));
}

void Space::update() {
    // Redemarre le chrono est retourne le temps �coul� depuis le dernier restart (en secondes)
    float loopDuration = chrono.restart().asSeconds();
    //Mise � jours des elements � afficher
    for (int i = 0; i < elements.size(); i++) {
        elements[i]->update(loopDuration);
    }
}

void Space::collision() {
    //Test de collision entre les �l�ments du jeu
    for(int i = 0; i < elements.size(); i++) {
        for (int j = 0; j < elements.size(); j++) {
            if (i != j) {
                elements[i]->collisionTest(*elements[j]);
            }
        }
    }
}

void Space::display(sf::RenderWindow& mainWindow) const {
    //Affichage des elements
    for (auto& element : elements) {
        element->display(mainWindow);
    }
}

void Space::cleanup() {
    if (toClear) {
        elements.clear();
        toClear = false;
    }

    auto arrayEnd = std::remove_if(std::begin(elements), std::end(elements), SpaceElement::isDestroy);
    elements.erase(arrayEnd, std::end(elements));
    for (auto& element : toAdd) {
        elements.push_back(std::move(element));
    }
    toAdd.clear();
}
 
void Space::clear() {
    toClear = true;
}