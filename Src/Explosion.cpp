#include "Explosion.h"
#include "ResourceManager.h"

Explosion::Explosion(Coordinates const& p_position) : SpaceElement{ "ressources/explosion.png" } {

	position = p_position;
	sound.setBuffer(ResourceManager<sf::SoundBuffer>::getResource("ressources/explosion.wav"));
	sound.setVolume(70);
	sound.play();
}


void Explosion::update(float duration) {

		age += duration; 

	if (age < LIFETIME) {
		spaceElementSprite.setScale(age / LIFETIME, age / LIFETIME);
	}else{
		//Permet de faire disparaitre l'explosion de l'�cran pendant que le son se termine.
		spaceElementSprite.setScale( 0,0 );
		//L'objet est d�truit uniquement lorsque le son � termin� de jouer (On s'assure donc que le son de l'explosion est jou� enti�rement)
		if (sound.getStatus() == sf::SoundSource::Stopped) {
			destroy = true;
		}
	}

	SpaceElement::update(duration);
}


void Explosion::collision(ElementType otherType) {

}