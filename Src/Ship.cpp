#include <iostream>
#include "Ship.h"
#include "Missiles.h"
#include "ResourceManager.h"


// Constructeur
Ship::Ship(Game& p_game, Space& p_space, sf::Color const& color) : SpaceElement("ressources/ship.png"), game{ p_game }, space{ p_space } {
	type = ElementType::SHIP;
	spaceElementSprite.setColor(color);
	}


// Destructeur
Ship::~Ship() {
	std::cout << "Au revoir !" << std::endl;
}

void Ship::refreshState(){
	//D�placements
	accelerating = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
	rightTurn = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
	leftTurn = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
	backward = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
	//Tirs
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && lastShot.getElapsedTime().asSeconds() > 0.25) {
		laserSound.setBuffer(ResourceManager<sf::SoundBuffer>::getResource("Ressources/laser.wav"));
		laserSound.setVolume(20);
		laserSound.play();
		space.add(std::make_unique<Missiles>(position, spaceElementSprite.getRotation()));
		lastShot.restart();
	}
}

void Ship::update(float duration) {
	refreshState();
	if (!destroy) {
		if (accelerating) {
			speed += Vector::directionAngle(ACCELERATION * duration, spaceElementSprite.getRotation());
		}
		if (backward) {
			speed -= Vector::directionAngle(ACCELERATION * duration, spaceElementSprite.getRotation());
		}
		// Formule de d�c�l�ration
		speed -= speed * FRICTION_COEFF * duration;
		//G�re la rotation du sprite via la fonction rotate de la SFML. Elle prend en argument un angle en degr�, mesur� dans le sens des aiguille d'une montre
		if (rightTurn) {
			angularSpeed = ANGULAR_SPEED;
		}
		else if (leftTurn) {
			angularSpeed = -ANGULAR_SPEED;
		}
		else
		{
			angularSpeed = 0;
		}
	}
	SpaceElement::update(duration);
}

void Ship::collision(ElementType otherType) {
	if (otherType == ElementType::ASTEROID) {
		destroy = true;
		game.gameOver();
		space.add(std::make_unique<Explosion>(position));		
	}
}
