#include "Coordinates.h"
#include "Vector.h"
#include <cmath>
#include <algorithm>


int Coordinates::widthLimit = 0;
int Coordinates::heightLimit = 0;

Coordinates::Coordinates(float pX, float pY) {
	x = pX;
	y = pY;
}

float Coordinates::getX() {
	return x;
}

float Coordinates::getY() {
	return y;
}


void Coordinates::operator+=(Vector const& other) {
	x += other.x;
	y += other.y;
	calculate();
}

void Coordinates::initializeSpace(int const& WIDTH, int const& HEIGHT) {
	widthLimit = WIDTH;
	heightLimit = HEIGHT;
}

int Coordinates::getWidthLimit() {
	return widthLimit;
}

int Coordinates::getHeightLimit() {
	return heightLimit;
}

void Coordinates::calculate() {
	if (x > widthLimit) {
		x -= widthLimit;
	}
	if (x < 0) {
		x += widthLimit;
	}
	if (y > heightLimit) {
		y -= heightLimit;
	}	
	if (y < 0) {
		y += heightLimit;
	}
}

float Coordinates::distanceCalculation(Coordinates const& other) const {
	Vector delta = Vector{ std::min({abs( x - other.x), abs(x - other.x - widthLimit), abs(x - other.x + widthLimit)}),
						   std::min({abs(y - other.y), abs(y - other.y - heightLimit), abs(y - other.y + heightLimit)}) };
	return sqrt(delta.x * delta.x + delta.y * delta.y);
}