#include "Vector.h"
#include <cmath>
#include <iostream>


const double M_PI = 3.14159265;

void Vector::operator+=(Vector const& other) {
	x += other.x;
	y += other.y;
}

void Vector::operator-=(Vector const& other) {
	x -= other.x;
	y -= other.y;
}

Vector Vector::operator*(float coefficient) const {
	return { x * coefficient, y * coefficient };
}

Vector Vector::directionAngle(float size, float angle) {
	return { size * static_cast<float> (cos(angle / 180.f *M_PI)), size * static_cast<float>(sin(angle / 180.f * M_PI)) }; //l'angle est convertie en degr�s (cos et sin sont en radian)
}
