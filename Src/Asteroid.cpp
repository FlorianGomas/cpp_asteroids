#include "Asteroid.h"
#include <iostream>
#include <random>
#include "Explosion.h"


//On rajoute dans la liste d'initilisation du constructeur le contructeur de la classe m�re, pour lui indiquer le chemin de chargement de l'image
Asteroid::Asteroid(Game& p_game, Space& p_space, Asteroid* parent) : SpaceElement("ressources/asteroid.png"), game{p_game}, space{ p_space }
{
	type = ElementType::ASTEROID;
	std::random_device generator;
	std::uniform_real_distribution <float> positionDistribution{ -150,150 };
	std::uniform_real_distribution <float> speedDistribution{ 80,120 };
	std::uniform_real_distribution <float> angleDistribution{ 0,360 };
	std::uniform_real_distribution <float> angularSpeedDistribution{ 10,30 };
	speed = Vector::directionAngle(speedDistribution(generator), angleDistribution(generator));
	angularSpeed = angularSpeedDistribution(generator);
	if (parent) {
		spaceElementSprite.setScale(parent->spaceElementSprite.getScale().x/1.4, parent->spaceElementSprite.getScale().y/1.4);
		position = parent->position;
	}
	else {
		position = { positionDistribution(generator), positionDistribution(generator) };
	}
}

void Asteroid::collision(ElementType otherType)  {
	if (otherType == ElementType::MISSILES) {
		destroy = true;
		game.addPoints(spaceElementSprite.getScale().x * 50);
		if (spaceElementSprite.getScale().x > 0.5) {
			space.add(std::make_unique<Asteroid>(game,space, this));
			space.add(std::make_unique<Asteroid>(game,space, this));
		}
		space.add(std::make_unique<Explosion>(position));
	}
}