#include "Game.h"
#include "Ship.h"
#include "Asteroid.h"
#include "Background.h"
#include <memory>
#include "ResourceManager.h"
#include "Font.h"
#include <string>
#include <fstream>

using namespace std::string_literals;

Game::Game(Space& p_space) : space{ p_space } {

    if (!font.loadFromMemory(Air_Americana_ttf, Air_Americana_ttf_size)) throw std::runtime_error{ "Police introuvable" };
    scoreText.setFont(font);
    bestScoreText.setFont(font);
    scoreText.move(0, 30);
    std::ifstream inputScoreFile{ "Scores.txt" };
    if (inputScoreFile.is_open()) {
        inputScoreFile >> bestScore;
    }
    inputScoreFile.close();
    refreshBestScore();
    //gestion d'erreur avec le try and catch
    try {
        startScreenSprite.setTexture(ResourceManager<sf::Texture>::getResource("Ressources/home.png"));
    }
    catch (std::exception const& exception) {
        initializeException(exception);
    }
}

void Game::addPoints(int points) {
    score += points;
    textRefresh();
}

void Game::textRefresh() {
    scoreText.setString("Score : "s + std::to_string(score));
}

void Game::refreshBestScore() {
    bestScoreText.setString("Meilleur score : "s + std::to_string(bestScore));
}

void Game::start() {
	onGoing = true;
    score = 0;
    textRefresh();
    // Creation de l'objet li� au pointeur unique & ajout de l'objet dans le tableau
    space.add(std::make_unique<Background>(space));
    space.add(std::make_unique<Ship>(*this, space, sf::Color::White));
    space.add(std::make_unique<Asteroid>(*this, space));
    space.add(std::make_unique<Asteroid>(*this, space));
    space.add(std::make_unique<Asteroid>(*this, space));
    space.add(std::make_unique<Asteroid>(*this, space));
}

void Game::gameOver() {
    onGoing = false;
    if (score > bestScore) {
        bestScore = score;
        refreshBestScore();
        std::ofstream outputScoreFile{"Scores.txt"};
        if (outputScoreFile.is_open()) {
            outputScoreFile << bestScore;
        }
        else {
            throw std::runtime_error{ "Impossible d'ouvrir le fichier Score en �criture" };
        }
        outputScoreFile.close();
    }
    space.clear();
}

void Game::display(sf::RenderWindow& startScreen) const {
    if (exceptionText) {
        startScreen.draw(*exceptionText);
    }
    else {
        if (!onGoing && space.isClear()) {
            startScreen.draw(startScreenSprite);
        }
        else {
            startScreen.draw(scoreText);
        }
        startScreen.draw(bestScoreText);
    }
}

void Game::initializeException(std::exception const& exception) {
    exceptionText = std::make_unique<sf::Text>();
    exceptionText->setFont(font);
    exceptionText->setString(exception.what());
    exceptionText->setFillColor(sf::Color::Red);
}