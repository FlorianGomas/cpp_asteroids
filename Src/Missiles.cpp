#include "Missiles.h"
#include "ResourceManager.h"

Missiles::Missiles(Coordinates const& p_position, float rotation) : SpaceElement{ "Ressources/missile.png" } {
	type = ElementType::MISSILES;
	position = p_position;
	spaceElementSprite.setRotation(rotation);
	speed = Vector::directionAngle(SPEED, rotation);
}

void Missiles::collision(ElementType otherType) {
	if (otherType != ElementType::SHIP && otherType != ElementType::BACKGROUND && otherType != ElementType::OTHERS) {
		destroy = true;
	}
}