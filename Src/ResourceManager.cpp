#include "ResourceManager.h"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

// Etant donn� l'attribut static  "resources" pr�sent dans le header, il faut le d�finir de la fa�on suivante pour chaque utilisation
template <>
std::unordered_map<std::string_view, sf::Texture> ResourceManager<sf::Texture>::resources{};

template <>
std::unordered_map<std::string_view, sf::SoundBuffer> ResourceManager<sf::SoundBuffer>::resources{};