#include "ExceptionResourceNotFound.h"

using namespace std::string_literals;

ExceptionResourceNotFound::ExceptionResourceNotFound(std::string_view const& path) : errorMessage { "Erreur, ressource introuvable : "s + path.data() } {

}

const char* ExceptionResourceNotFound::what() const noexcept  {
	return errorMessage.c_str();
}