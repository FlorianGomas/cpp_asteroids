// Asteroids.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <vector>
#include "Space.h"
#include "Vector.h"
#include "Coordinates.h"
#include "Background.h"
#include <SFML/Graphics.hpp> //Header pour la SFML
#include "Game.h"
#include <exception>
#include <SFML/Audio.hpp>

constexpr int WINDOW_WIDTH = 1280;
constexpr int WINDOW_HEIGHT = 1024;

int main()
{
    //Creation d'une fenetre (via SFML)
    sf::RenderWindow mainWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Asteroids");
    Coordinates::initializeSpace(WINDOW_WIDTH, WINDOW_HEIGHT);
    //Creation d'un tableau d'éléments
    Space space = Space{};
    Game game = Game{ space };
    sf::Music music;
    music.openFromFile("ressources/Asteroids.wav");
    music.setVolume(90);
    music.play();
    music.setLoop(true);

    //Tant que la fenetre est ouverte, on l'affiche
    while (mainWindow.isOpen()) {
        try {
            sf::Event event;

            while (mainWindow.pollEvent(event)) {
                //Gestion de la fermeture de la fenetre
                if (event.type == sf::Event::Closed) {
                    mainWindow.close();
                }
                //Demarrage d'une partie
                if (event.type == sf::Event::KeyPressed && !game.inProgress()) {
                    if (event.key.code == sf::Keyboard::Enter) {
                        game.start();
                    }
                    if (event.key.code == sf::Keyboard::Escape) {
                        exit(0);
                    }
                }
            }

            space.update();
            space.collision();

            //Il faut toujours clear la fenetre avant d'afficher qque chose (dans le cas de la SFML). On choisi la couleur de la fenetre dans le clear
            mainWindow.clear();
            space.display(mainWindow);
            game.display(mainWindow);
            mainWindow.display();

            space.cleanup();
        }
        catch (std::exception const& exception) {
            game.initializeException(exception);
        }
    }
    return EXIT_SUCCESS;
}
